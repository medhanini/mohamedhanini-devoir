#include <stdio.h>


int count_char(char *str, char ch) {
    int te = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == ch) {
            te++;
        }
    }
    return te;
}


int count_words(char *str, char *delimiter) {
    int te = 1; 
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == delimiter[0] && str[i+1] != delimiter[0]) {
            te++;
        }
    }
    return te;
}


int count_words_better(char *str, char *delimiter) {
    int te = 0;
    int word = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] != delimiter[0] && word) {
            te++;
            word = 1;
        } else if (str[i] == delimiter[0]) {
            word = 0;
        }
    }
    return te;
}

int main() {
    char input_str[50];
    printf("Entrez une chaîne  (maximum 49 caractères) :\n");
    scanf("%49s", input_str);
    printf("La chaîne saisie  : %s\n", input_str);

    char ch;
    printf("Entrez un caractère  :\n");
    scanf(" %c", &ch);
    int te = count_char(input_str, ch);
    printf("Le caractère %c apparaît %d fois dans la chaîne.\n", ch, te);

    int word_te = count_words(input_str, " ");
    printf("La chaîne à %d mots.\n", word_te);

    int word_te_better = count_words_better(input_str, " ");
    printf("La chaîne à %d mots ( prenant les espaces consécutifs).\n", word_te_better);

    return 0;
}
