#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void small_to_zero(int *t, int n, int val) {
    for(int i = 0; i < n; i++) {
        if(t[i] <= val) {
            t[i] = 0;
        }
    }
}

void add_vectors(int n, double *t1, double *t2, double *t3) {
    for(int i = 0; i < n; i++) {
        t3[i] = pow((t1[i] - t2[i]), 2);
    }
}

double norm_vector(int n, double *t) {
    double sum = 0;
    for(int i = 0; i < n; i++) {
        sum += pow(t[i], 2);
    }
    return sqrt(sum);
}

int main() {
    int tab[] = {1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0};
    int n = 11;
    int val = 3;
    printf("Tableau 1: ");
    for(int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
    small_to_zero(tab, n, val);
    printf("Tableau  de small_to_zero : ");
    for(int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
    
    double T1[] = {3.14, -1.0, 2.3, 0, 7.1};
    double T2[] = {2.71, 2.5, -1, 3, -7};
    double T3[5];
    printf("T1 : ");
    for(int i = 0; i < n; i++) {
        printf("%+.2lf ", T1[i]);
    }
    printf("\nT2 : ");
    for(int i = 0; i < n; i++) {
        printf("%+.2lf ", T2[i]);
    }
    printf("\n");
    add_vectors(n, T1, T2, T3);
    printf("T3 (résult add_vectors) : ");
    for(int i = 0; i < n; i++) {
        printf("%+.2lf ", T3[i]);
    }
    printf("\n");

    double norm_T1 = norm_vector(n, T1);
    double norm_T2 = norm_vector(n, T2);
    double norm_T3 = norm_vector(n, T3);
    printf("Norme de T1 : %.2lf\n", norm_T1);
    printf("Norme de T2 : %.2lf\n", norm_T2);
    printf("Norme de T3 : %.2lf\n", norm_T3);

    exit1;
}

