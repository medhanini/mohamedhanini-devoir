#include<stdio.h>
#include<stdlib.h>

typedef struct node node;

struct node {
   int val;
   node *next;
   node *prev; 
};

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);
   
    if (head == NULL) {
        head = newnode;
    } else {
        walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
        newnode->prev = walk; 
    }
    return head;
}

void forth_and_back(node *head) {
    node *walk = head;

    while (walk != NULL) { 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");

    walk = head;
    while (walk->next != NULL) { 
        walk = walk->next;
    }

    while (walk != NULL) { 
        printf("%d ", walk->val);
        walk = walk->prev;
    }
    printf("\n");
}

int main() {
    node *head1 = NULL;
    node *head2 = NULL;

    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, 54);
    head1 = append_val(head1, 41);

    append_val(head2, 42);
    append_val(head2, 12);
    append_val(head2, 54);
    append_val(head2, 41);

    print_list(head1);
    print_list(head2);

    forth_and_back(head1);
    forth_and_back(head2);

    return 0;
}

